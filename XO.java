import java.util.InputMismatchException;
import java.util.Scanner;

public class XO {
	public static Scanner kb = new Scanner(System.in);
	static String[][] table = { { "_", "_", "_" }, 
								{ "_", "_", "_" }, 
								{ "_", "_", "_" } };
	
	
	static String turn = "O";
	public static int count = 0;
	
	public static void showTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print("|"+table[i][j] + "|");
            }
            System.out.println();
        }

    }
	public static void showWelcome(){
        System.out.println("Welcome to XO Game");
    }
	public static void input() {
        
        System.out.print("input row and column : ");
        try {
            int inputRow = kb.nextInt();
            int inputCol = kb.nextInt();
            if (checkError(inputRow, inputCol) == true) {
                input();
            } else {
                inputTableOX(inputRow, inputCol);
            }
        } catch (InputMismatchException e) {
            System.out.println("Input mismatch , try again");
        }
    }
	public static boolean checkError(int inputRow, int inputCol) {
        if (inputRow > 3 || inputRow < 1 || inputCol > 3 || inputCol < 1) {
            System.out.println("Error!! Row : Please input 1 or 2 or 3");
            return true;
        } else if (table[inputRow - 1][inputCol - 1] == "O"
                || table[inputRow - 1][inputCol - 1] == "X") {
            System.out.println("You can't select this location "
                    + "because it has already been chosen.");
            return true;
        }
        return false;
    }
	public static void inputTableOX(int inputRow, int inputCol) {
        table[inputRow - 1][inputCol - 1] = turn;
        count++;
        checkWin();
        if (checkWin() == false) {
            switchPlayer();
        }

    }
	
    public static void showTurn(){
    	System.out.println(turn+" turn");
    	input();
    }
    public static void switchPlayer() {
        if (turn == "O") {
            turn = "X";
        } else if (turn == "X") {
            turn = "O";
        }
    }
    public static boolean checkWin() {
        if (checkRow()) {
            return true;
        } else if (checkCol()) {
            return true;
        } else if (checkWinCrosswise()) {
            return true;
        }
        return false;
    }
    public static boolean Draw() {
        if (checkDraw()) {
            return true;
        }
        return false;
    }

    public static boolean checkDraw() {
        if (count == 9 && checkWin() == false) {
            return true;
        }
        return false;
    }
    public static boolean checkRow() {
        if (table[0][0] == turn && table[0][1] == turn
                && table[0][2] == turn) {
            return true;
        } else if (table[1][0] == turn && table[1][1] == turn
                && table[1][2] == turn) {
            return true;
        } else if (table[2][0] == turn && table[2][1] == turn
                && table[2][2] == turn) {
            return true;
        }

        return false;
    }
    public static boolean checkCol() {
        if (table[0][0] == turn && table[1][0] == turn
                && table[2][0] == turn) {
            return true;
        } else if (table[0][1] == turn && table[1][1] == turn
                && table[2][1] == turn) {
            return true;
        } else if (table[0][2] == turn && table[1][2] == turn
                && table[2][2] == turn) {
            return true;
        }

        return false;
    }
    public static boolean checkWinCrosswise() {
        if (checkCrosswise1()) {
            return true;
        } else if (checkCrosswise2()) {
            return true;
        }
        return false;
    }

    public static boolean checkCrosswise1() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != turn) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkCrosswise2() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != turn) {
                return false;
            }
        }
        return true;
    }
    public static void showBye(){
        System.out.println("Bye Bye");
    }
    public static void showWinner() {
        if (checkWin()) {
            showTable();
            System.out.println("Player " + turn + " is winner!!\n");
        } else if (Draw()) {
            showTable();
            System.out.println("Player O ties Player X!!\n");
        }
    }
	public static void main(String[] args) {
		showWelcome();
        
        while (true) {
            showTable();
            showTurn();
            if (checkWin() || Draw()) {
                break;
            }

        }
        showWinner();

	}
	

}
